Ergodox EZ Advantage Nordic Enhanced
====================================

Thanks to the COVID-19 pandemic and having to
work at home without a proper desk or anything
to work at, I got to enjoy tweaking an Ergodox
keyboard.

As a long-time user of the Kinesis, in withdrawal
over not being able to have it around now, this
is as close as I've come so far. Some of the
right-hand alt-gr keys are awkward for my hands,
but this is as close as I've gotten to a good
stand-in.

Much respect to `FalbaTech <https://falba.tech/>`_
for constructing my keyboard!

Usage
-----

I don't want to bother forking the `QMK repo <https://github.com/qmk/qmk_firmware>`_
just to have a branch for this.

This should be used as a submodule, or if I come
up with some way of doing out-of-tree builds, that's
probably just as good.

Some notes
~~~~~~~~~~

Here are some notes mainly for myself, as the likely
sole user, who updates this so seldom it makes sense
to have some pointers in the right direction.

  * Have qmk at ``$HOME/qmk_firmware/``
  * Have submodule at ``$HOME/qmk_firmware/keyboards/ergodox_ez/keymaps/ergodox-ez-advantage-nordic-enhanced/``
  * Have a ``qmk`` virtualenv
  * Have ``qmk==1.1.0`` in the virtualenv

After downloading from Oryx, this should help to get rid
of some incompatible code

.. code:: bash

   patch -p1 -R < revert-after-oryx-download.patch

In the top directory, compile with

.. code:: bash

   PATH=$HOME/qmk_firmware/bin/:$PATH qmk compile -kb ergodox_ez -km ergodox-ez-advantage-nordic-enhanced

Upstream
--------

I have the main configuration at `Oryx <https://configure.ergodox-ez.com/ergodox-ez/layouts/wNeeq/latest>`_
but because configuring the LEDs is not possible
there, I sync up the changes here.

Note the online configurator may use a different
version of QMK because the downloaded source wouldn't
compile out of the box.

Ergodox EZ Advantage Nordic Enhanced is proven to work
with commit `a716517705 <https://github.com/qmk/qmk_firmware/commit/a716517705>`_
as the base. Add the submodule etc on top of that commit.

